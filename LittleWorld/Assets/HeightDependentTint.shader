﻿Shader "Custom/HeightDependentTint" 
 {
   Properties 
   {
     _MainTex ("Base (RGB) Alpha (A)", 2D) = "white" {}
     _HeightMin ("Height Min", Float) = -1
     _HeightMax ("Height Max", Float) = 1
     _ColorMin ("Tint Color At Min", Color) = (0,0,0,1)
     _ColorMax ("Tint Color At Max", Color) = (1,1,1,1)
   }
  
   SubShader
   {
     Tags { "Queue"="Transparent" "RenderType"="Transparent" }
  
     CGPROGRAM
     #pragma surface surf Lambert alpha
  
     sampler2D _MainTex;
     fixed4 _ColorMin;
     fixed4 _ColorMax;
     float _HeightMin;
     float _HeightMax;
  
     struct Input
     {
       float2 uv_MainTex;
       float3 worldPos;
     };
  
     void surf (Input IN, inout SurfaceOutput o) 
     {
       //half4 c = tex2D (_MainTex, IN.uv_MainTex);
	   half4 c= half4(0.93,0.7,0.5,1);
       float h = (_HeightMax-IN.worldPos.y) / (_HeightMax-_HeightMin);
       fixed4 tintColor = lerp(_ColorMax.rgba, _ColorMin.rgba, h);
       o.Albedo = c.rgb; // * tintColor.rgb;
       o.Alpha = c.a * tintColor.a;

	   //o.Albedo = float3(1,0,0);
       //o.Alpha = 0.5;

     }
     ENDCG
   } 
   Fallback "Diffuse"
 }