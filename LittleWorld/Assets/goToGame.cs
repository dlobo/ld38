﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class goToGame : MonoBehaviour {

    // Use this for initialization
    void Awake()
    {
        Screen.SetResolution(405, 720, false);
    }

    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void PlayGame(){
		SceneManager.LoadScene ("Game");
	}

	public void GoMenu()
	{
		SceneManager.LoadScene ("Menu");
	}
}
