﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BootVisibility : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnBecameInvisible()
    {
        Debug.Log("OnBecameInvisible");
        transform.position = new Vector3(transform.position.x,Camera.main.transform.position.y + 10f, transform.position.z);
    }
}
