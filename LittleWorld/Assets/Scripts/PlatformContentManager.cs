﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlatformContentManager : MonoBehaviour {

    public WorldPieceManager[] worldPieceTemplates;

    public VehicleManager[] vehicleTemplates;
    public HumanManager[] humanTemplates;

    public float worldPieceWidth = 2;
    public float worldPieceHeight = 2;
    public float tileSize = 10;
    public int bufferSize = 16;

    public float spawnDistance = 4;

    public float vehicleMinWait = 2;
    public float vehicleMaxWait = 6;
    public float vehicleMinWaitPro;
    public float vehicleMaxWaitPro;
    private float vehicleTimerIni;
    private float vehicleTimerMax;

    public float humanMinWait = 1;
    public float humanMaxWait = 3;
    public float humanMinWaitPro;
    public float humanMaxWaitPro;
    private float humanTimerIni;
    private float humanTimerMax;

    private int currentInc = 0;
    public int distIncrement = 25;
    public float factIncrement = 0.99f;

	public AudioClip[] humanSqueezeSounds;
    public AudioClip[] giantVoices;
    public AudioClip[] carStompSounds;
	public ParticleSystem carExplosion;

    public float deleteDistance = 4;

    public float worldVelocity = -100f;

    private List<HumanManager> activeHumans;
    private List<VehicleManager> activeVehicles;
    private List<WorldPieceManager> activePieces;

    private float lastVoice = 0.0f;
    private float voiceWait = 2.5f;

    public GameController gameController;

    private float totalDistance = 0;

	// Use this for initialization
	void Start ()
    {
        lastVoice = 0;

        totalDistance = 0;

        this.humanMinWaitPro = this.humanMinWait;
        this.humanMaxWaitPro = this.humanMaxWait;
        this.vehicleMinWaitPro = this.vehicleMinWait;
        this.vehicleMaxWaitPro = this.vehicleMaxWait;
        this.currentInc = 0;

        activePieces = new List<WorldPieceManager>();
        activeVehicles = new List<VehicleManager>();
        activeHumans = new List<HumanManager>();
       
        for (int i = 0; i < bufferSize; ++i)
        {
            GameObject newWorldPiece = this.createRandomPiece();
            newWorldPiece.transform.position = this.transform.position + new Vector3(-worldPieceHeight * tileSize + worldPieceHeight * i * tileSize, 0, 0);
            this.activePieces.Add(newWorldPiece.GetComponent<WorldPieceManager>());
        }

        Debug.Log(gameController.currentBalance);
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (gameController.currentPoints > this.currentInc * this.distIncrement)
        {
            this.currentInc++;
            this.humanMinWaitPro *= this.factIncrement;
            this.humanMaxWaitPro *= this.factIncrement;
            this.vehicleMinWaitPro *= this.factIncrement;
            this.vehicleMaxWaitPro *= this.factIncrement;
            Debug.Log("DIFFICULTY INCREMENT: " + this.currentInc);
        }

        float worldStep = Time.fixedDeltaTime * worldVelocity;
        float lastPieceX = -Mathf.Infinity;
        Transform lastPieceT = null;

        this.totalDistance -= worldStep;
        if (totalDistance < 0)
            this.totalDistance = 0;

        this.gameController.setPoints((int) totalDistance);

        // Move world pieces

        for (int i = 0; i < bufferSize; ++i)
        {
            this.activePieces[i].transform.position += new Vector3(worldStep, 0, 0);

            if (this.activePieces[i].transform.position.x > lastPieceX)
            {
                lastPieceX = this.activePieces[i].transform.position.x;
                lastPieceT = this.activePieces[i].transform;
            }
        }

        // Swap old world pieces

        for (int i = 0; i < bufferSize; ++i)
        {
            if (this.activePieces[i].transform.position.x + deleteDistance * worldPieceHeight * tileSize < this.transform.position.x)
            {
                //Debug.Log("Deleted world piece: " + i);
                Destroy(this.activePieces[i].gameObject);
                this.activePieces[i] = this.createRandomPiece().GetComponent<WorldPieceManager>();
                this.activePieces[i].transform.position = lastPieceT.position + new Vector3(worldPieceHeight * tileSize, 0, 0);
            }
        }

        bool[] vdeleteVehicles = new bool[this.activeVehicles.Count];
        bool[] vdeleteHumans = new bool[this.activeHumans.Count];

        // Delete old vehicles

        for (int i = 0; i < this.activeVehicles.Count; ++i)
        {
            if (this.activeVehicles[i] == null)
            {
                vdeleteVehicles[i] = true;
            }
            else if (this.activeVehicles[i].transform.position.x + deleteDistance * worldPieceHeight * tileSize < this.transform.position.x)
            {
                vdeleteVehicles[i] = true;
                //Debug.Log("Deleted vehicle: " + i);
                Destroy(this.activeVehicles[i].gameObject);
            }
            else
            {
                vdeleteVehicles[i] = false;
            }
        }

        for (int i = this.activeVehicles.Count - 1; i >= 0; --i)
            if (vdeleteVehicles[i])
                this.activeVehicles.RemoveAt(i); // Remove backwards

        // Delete old humans

        for (int i = 0; i < this.activeHumans.Count; ++i)
        {
            if (this.activeHumans[i] == null)
            {
                vdeleteHumans[i] = true;
            }
            else if (this.activeHumans[i].transform.position.x + deleteDistance * worldPieceHeight * tileSize < this.transform.position.x)
            {
                vdeleteHumans[i] = true;
                //Debug.Log("Deleted human: " + i);
                Destroy(this.activeHumans[i].gameObject);
            }
            else
            {
                vdeleteHumans[i] = false;
            }
        }

        for (int i = this.activeHumans.Count - 1; i >= 0; --i)
            if (vdeleteHumans[i])
                this.activeHumans.RemoveAt(i); // Remove backwards

        // Move vehicles 

        //Debug.Log("Active vehicles: " + this.activeVehicles.Count);

        for (int i = 0; i < this.activeVehicles.Count; ++i)
        {
            this.activeVehicles[i].transform.position += new Vector3(worldStep, 0, 0);
        }

        // Move humans

        //Debug.Log("Active humans: " + this.activeHumans.Count);

        for (int i = 0; i < this.activeHumans.Count; ++i)
        {
            this.activeHumans[i].transform.position += new Vector3(worldStep, 0, 0);
        }

        // Spawn new vehicles if needed

        if (Time.fixedTime - this.vehicleTimerIni >= this.vehicleTimerMax)
        {
            this.vehicleTimerIni = Time.fixedTime;
            this.vehicleTimerMax = Random.Range(this.vehicleMinWaitPro, 
                                                this.vehicleMaxWaitPro);
            bool side = Random.Range(0f, 1f) > 0.5;
            Vector3 deltaPos;
            if (side)
                deltaPos = new Vector3(this.spawnDistance*this.worldPieceHeight*this.tileSize, 0, -tileSize / 4);
            else deltaPos = new Vector3(this.spawnDistance*this.worldPieceHeight*this.tileSize, 0, +tileSize / 4);

            GameObject newVehicle = this.createRandomVehicle();
            newVehicle.transform.position += deltaPos;
            this.activeVehicles.Add(newVehicle.GetComponent<VehicleManager>());
        }
        else {  /* Just wait */ }

        // Spawn new humans if needed

        if (Time.fixedTime - this.humanTimerIni >= this.humanTimerMax)
        {
            this.humanTimerIni = Time.fixedTime;
            this.humanTimerMax = Random.Range(this.humanMinWaitPro,
                                              this.humanMaxWaitPro);
            bool side = Random.Range(0f, 1f) > 0.5;
            Vector3 deltaPos;
            if (side)
                deltaPos = new Vector3(this.spawnDistance * this.worldPieceHeight * this.tileSize, 0, -tileSize / 4);
            else deltaPos = new Vector3(this.spawnDistance * this.worldPieceHeight * this.tileSize, 0, +tileSize / 4);

            GameObject newHuman = this.createRandomHuman();
            newHuman.transform.position += deltaPos;
            this.activeHumans.Add(newHuman.GetComponent<HumanManager>());
        }
        else {  /* Just wait */ }
    }

    private GameObject createRandomPiece()
    {
        int templateNum = Random.Range(0, this.worldPieceTemplates.Length);

        //Debug.Log("Random piece generated: " + templateNum);
        WorldPieceManager pieceManagerTemplate = worldPieceTemplates[templateNum];
        GameObject newWorldPiece = Instantiate(pieceManagerTemplate.gameObject, this.transform);
        return newWorldPiece;
    }

    private GameObject createRandomVehicle()
    {
        int templateNum = Random.Range(0, this.vehicleTemplates.Length);

        //Debug.Log("Random vehicle generated: " + templateNum);
        VehicleManager vehicleManagerTemplate = vehicleTemplates[templateNum];
        GameObject newVehicle = Instantiate(vehicleManagerTemplate.gameObject, this.transform);
        newVehicle.GetComponent<VehicleManager>().platformManager = this;
        return newVehicle;
    }

    private GameObject createRandomHuman()
    {
        int templateNum = Random.Range(0, this.humanTemplates.Length);

        //Debug.Log("Random human generated: " + templateNum);
        HumanManager humanManagerTemplate = humanTemplates[templateNum];
        GameObject newhuman = Instantiate(humanManagerTemplate.gameObject, this.transform);
        newhuman.GetComponent<HumanManager>().platformManager = this;
        return newhuman;
    }

    public void killHuman(HumanManager human)
    {
        // Spawn effects

        human.GetComponent<HumanManager>().isDead = true;
        human.transform.GetChild(1).gameObject.SetActive(true);
        human.transform.GetChild(0).gameObject.SetActive(false);
        human.transform.GetChild(0).GetComponent<Animator>().SetBool("Dead", true);

        // Destroy the object
		//efecto muerte humano

		int randomSqueeshSound = Random.Range(0,2);
		this.GetComponent<AudioSource> ().PlayOneShot (humanSqueezeSounds[randomSqueeshSound], 0.9f);

        if (Time.time - lastVoice > voiceWait)
        {
            this.GetComponent<AudioSource>().PlayOneShot(giantVoices[Random.Range(0, 8)], 2f);
            lastVoice = Time.time;
        }

        Destroy(human.gameObject, 5.0f);
    }

    public void killVehicle(VehicleManager vehicle)
    {
        // Spawn effects

        this.gameController.reduceHealth(this.gameController.damageVehicleHit);

        // TODO
		bool notExploded = true;
        // Destroy the object
		//efecto explosion coche
		if (notExploded) {
			ParticleSystem explosion = Instantiate (carExplosion, vehicle.gameObject.transform) as ParticleSystem;
			explosion.gameObject.transform.position = vehicle.transform.position; 
			explosion.gameObject.transform.parent = null;
			explosion.Play ();
			//explosionSound
			Destroy(explosion.gameObject, 1.5f);
			notExploded = false;
		}


		int carStompSound = Random.Range(0,2);
		this.GetComponent<AudioSource> ().PlayOneShot (carStompSounds[carStompSound], 0.5f);

        if (Time.time - lastVoice > voiceWait)
        {
            this.GetComponent<AudioSource>().PlayOneShot(giantVoices[Random.Range(0, 8)], 2f);
            lastVoice = Time.time;
        }

        Destroy(vehicle.gameObject, 0.0f);


    }

    public void collideVehicles(VehicleManager veh0, VehicleManager veh1)
    {
        // Remove from lists

        veh0.isBroken = true;
        veh1.isBroken = true;

        // Play effects

        // TO DO

        // Destoy the object

        Destroy(veh0.gameObject, 0.0f);
        Destroy(veh1.gameObject, 0.0f);
    }
}
