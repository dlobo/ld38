﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShadowControl : MonoBehaviour {

    public bool active = false;
    public bool ground = false;
    public float vel = 0f;
    public LayerMask lm_floor;

    public Vector3 pos;
    public float stepSize = 6.0f;

    public GameObject legProxy;
    public GameObject footProxy;

    public float timeActive;
    public float timeGround;
    float maxTimeActive = 2.5f;


    public GameController gameController = null;

    // Use this for initialization
    void Start () {
        timeActive = 0.0f;
        timeGround = 0.0f;
        ground = true;
        active = false;
	}
	
	// Update is called once per frame
	void Update () {

        if (active)
        {
            transform.position += new Vector3(vel, 0, 0) * Time.fixedDeltaTime;

            timeActive = 0.0f;
        }
        else
        {
            transform.position += new Vector3(vel, 0, 0) * Time.fixedDeltaTime;

            timeActive += Time.deltaTime;
        }

        if (ground)
        {
            timeGround += Time.deltaTime;
        }
        else
        {
            this.gameController.balancing = true;

            timeGround = 0.0f;
        }

        if (timeActive > 1.0f)
        {
            //float fov = GetComponent<Projector>().fieldOfView;
            //GetComponent<Projector>().fieldOfView = fov * ((Random.value*10)-5);


        }

            if (timeActive > maxTimeActive)
        {
            //Debug.Log("PERDISTE: DEMASIADO TIEMPO EN EQUILIBRIO. TE CAÍSTE NOMÁS");

            // ZZZ PERDISTE

        }

        if (ground)
        {
            GetComponent<Projector>().enabled = false;
        }
    }

    public void ChangeTurn()
    {
        active = !active;

        if (active)
        {
            //this.legProxy.SetActive(false);
            //this.footProxy.SetActive(true);

            GetComponent<Projector>().enabled = true;

            GetComponent<Collider>().enabled = true;

            //LAUNCH LEG ANIMATION

            ground = false;

            // Check surroundings for humans
        }
        else
        {
            //this.legProxy.SetActive(true);
            //this.footProxy.SetActive(false);

            GetComponent<Projector>().enabled = false;

            

            //GetComponent<Collider>().enabled = false;


            //RISE LEG

            ground = true;
        }


    }
}
