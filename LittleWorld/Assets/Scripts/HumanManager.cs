﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HumanManager : MonoBehaviour {

    public float accelMax = 5;
    public float speedMax = 5;
    public float curWeight = 0.5f;
    public float wndWeight = 0.5f;
    public float colWeight = 2.5f;
    public float forWeight = 0.1f;
    public float colDistan = 5.0f;

    public bool isDead = false;

    private Vector3 curVel;

    GameObject leftLimit = null;
    GameObject rightLimit = null;

    GameController gameController = null;

    public PlatformContentManager platformManager;

	// Use this for initialization
	void Start () {
        curVel = new Vector3(0.0f, 0.0f, 1.0f);
        leftLimit = GameObject.Find("CollisionPlaneLeft");
        rightLimit = GameObject.Find("CollisionPlaneRight");

        gameController = GameObject.Find("GameController").GetComponent<GameController>();

        this.isDead = false;
    }
	
	// Update is called once per frame
	void Update () {
        

        // Compute new direction

        Vector3 newDir = new Vector3(0.0f,0.0f,0.0f);

        // Current direction
        newDir += curWeight * curVel.normalized;

        // Wandering direction
        Vector3 wndDir = Random.onUnitSphere;
        wndDir.y = 0; wndDir.Normalize();
        newDir += wndWeight * wndDir;

        // Forward direction

        newDir += forWeight * new Vector3(-1.0f, 0.0f, 0.0f);

        // Collision direction
        RaycastHit hitInfo;

        if (Physics.Raycast(new Ray(transform.position, curVel.normalized), out hitInfo) && hitInfo.distance < colDistan)
        {
            if (hitInfo.collider.gameObject.CompareTag("Obstacle"))
            {
                Vector3 colDir = Vector3.Reflect(curVel.normalized, hitInfo.normal);
                colDir.y = 0; colDir.Normalize();
                newDir += colWeight * colDir;
            }

            if (hitInfo.collider.gameObject.CompareTag("Giant"))
            {
                Vector3 colDir = Vector3.Reflect(curVel.normalized, hitInfo.normal);
                colDir.y = 0; colDir.Normalize();
                newDir += colWeight * colDir;
            }
        }
        else
        {
            if (leftLimit.transform.position.z - transform.position.z < colDistan)
                newDir += colWeight * new Vector3(0.0f, 0.0f, -1.0f);

            if (transform.position.z - rightLimit.transform.position.z < colDistan)
                newDir += colWeight * new Vector3(0.0f, 0.0f, 1.0f);
        }

        newDir.Normalize();
        curVel = newDir * speedMax;
        //Debug.Log("New vel: " + curVel);

        if (!isDead)
        {
            transform.LookAt(transform.position + newDir);
            transform.position += newDir * speedMax * Time.fixedDeltaTime;  
        }
	}

    public void OnTriggerStay(Collider collider)
    {
        if (this.isDead)
            return;

        if (collider.gameObject.CompareTag("Giant"))
        {
            if (collider.gameObject.name.CompareTo("FootProxy") != 0)
                return;

            ShadowControl shadowControl = collider.gameObject.transform.parent.gameObject.GetComponent<ShadowControl>();
            if (shadowControl != null && shadowControl.ground && shadowControl.timeGround < 0.1)
            {
                this.platformManager.killHuman(this);
            }
        }
    }

}
