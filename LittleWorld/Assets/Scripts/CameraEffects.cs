﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraEffects : MonoBehaviour {

	public GameController gameRuler;
	private float balance, maxBalance, maxAmp;
	private Vector3 curr_position;


	// Use this for initialization
	void Start () {
		balance = 0.0f;
		maxBalance = gameRuler.maximumBalance;
		curr_position = transform.position;
		maxAmp = 3;
	}
	
	// Update is called once per frame
	void Update () {
		float maxAngle, sin_func, k, A;


		balance = gameRuler.currentBalance;

		A = maxAmp - (balance/maxBalance)*maxAmp;
		//amp = maxBalance - balance;
		k = 1;

		sin_func = Mathf.Cos (k * Time.time) * A;
		//sin_func = Mathf.Cos (Mathf.PI/2 - k);
		transform.position = curr_position + new Vector3 (0.0f, 0.0f, sin_func);			

	}
}
